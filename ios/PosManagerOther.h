
//
//  PosManagerOther.h
//  TestDevice
//
//  Created by Jellyfish on 2017/9/27.
//  Copyright © 2017年 Jellyfish. All rights reserved.


#ifndef PosManagerOther_h
#define PosManagerOther_h

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>


#pragma mark ----------------  ----------------

typedef enum {
    ErrorCodeScanDeviceError = 1,
    ErrorCodeConnectDeviceError = 2,
    ErrorCodeDisconnectDeviceError = 3,
    ErrorCodeExceptionDisconnectDeviceError = 4,
    ErrorCodeGetDeviceInfoError = 5,
    ErrorCodeGetTSKError = 6,
    ErrorCodeUpdateMasterKeyError = 7,
    ErrorCodeUpdateWorkingKeyError = 8,
    ErrorCodeAddAidError = 9,
    ErrorCodeAddRidError = 10,
    ErrorCodeGetCardNumberError = 11,
    ErrorCodeReadCardError = 12,
    ErrorCodeInputInfoError = 13,
    ErrorCodeReadPwdError = 14,
    ErrorCodeOnlineDataProcessError = 15,
    ErrorCodeSaveTradeDataError = 16,
    ErrorCodeCancelTradeError = 17,
    ErrorCodeCalculateMacError = 18,
    ErrorCodeUpdateFirmwareError = 19,
    ErrorCodeGetQrcodeError = 20,
    ErrorCodeGetQrcodeStrError = 21,
    ErrorCodeWriteInfoError = 22,
    ErrorCodeReadInfoError = 23,
    ErrorCodeGetBatteryError = 24,
} ErrorCode;


typedef enum {
    DeviceTypeHasKeyboard = 0,
    DeviceTypeNoKeyboard = 1,
} DeviceType;



typedef enum {
    LanguageTypeCH = 0,
    LanguageTypeTW = 1,
    LanguageTypeEN = 2,
} LanguageType;


typedef enum {
    TradeType_Deal = 1,
    TradeType_Balance = 2,
} TradeType;


typedef	enum{
    CardType_MAGNETIC = 0,
    CardType_ICC = 1,
    CardType_RF = 2,
} CardType;


typedef enum {
    InputTypeAmountFromKB = 0,
    InputTypePwdFromKB = 1,
    InputTypePwdFromNoKB = 2,
} InputType;



#pragma mark ----------------  ----------------


@interface DeviceInfo	: NSObject

@property (nonatomic, assign) DeviceType deviceType;

@property (nonatomic, copy) NSString *btName;

@property (nonatomic, copy) NSString *ksn;

@property (nonatomic, assign) CGFloat currentElePer;

@property (nonatomic, copy) NSString *firmwareVer;
@end


@interface ReadCardParamsInfo : NSObject

@property (nonatomic,assign) BOOL supportFallBack;

@property (nonatomic,assign) int timeout;

@property (nonatomic,copy) NSString *amount;

@property (nonatomic,assign) TradeType tradeType;

@property (nonatomic,copy) NSString *countryCode;

@property (nonatomic,assign) NSString * currencyCode;

@end


@interface CardDataInfo : NSObject

@property (nonatomic,assign) CardType cardType;

@property (nonatomic,copy) NSString *track1;

@property (nonatomic,copy) NSString *track2;

@property (nonatomic,copy) NSString *track3;

@property (nonatomic,copy) NSString	*cardno;

@property (nonatomic,copy) NSString	*experiyDate;

@property (nonatomic,copy) NSString	*panSerialNO;

@property (nonatomic,copy) NSString *data55;

@property (nonatomic,copy) NSString	*data59;

@property (nonatomic,copy) NSString	*tusn;

@property (nonatomic,copy) NSString	*encryptedSN;

@property (nonatomic,copy) NSString	*deviceType;

@property (nonatomic,copy) NSString	*iv;
@end


@interface InputInfo	: NSObject

@property (nonatomic, assign) InputType inputType;

@property (nonatomic, assign) int timeout;

@property (nonatomic,copy) NSString *info;
@end


@interface EMVOnlineData : NSObject

@property (nonatomic,copy)	NSString *responseCode;

@property (nonatomic,copy)	NSString *onlineData;
@end


#pragma mark ------------- 回调block定义 -------------


typedef void(^onScanResultCB)(CBPeripheral *device);

typedef void(^onVoidCB)();

typedef void(^onGetDeviceInfoCB)(DeviceInfo *deviceInfo);

typedef void(^onGetSessionKeyCB)(NSString *encrytedPubkey);

typedef void(^onGetCardNumberCB)(NSString *cardNumber);

typedef void(^onReadCardCB)(CardDataInfo *cardInfo);

typedef void(^onReadInputInfo)(NSString *info);

typedef void(^onNSStringCB)(NSString *stringCB);

typedef void(^onGetInfo)(NSString *info);

typedef void(^onGetCurrentBatteryStatu)(BOOL status);

#endif /* PosManagerOther_h */
