
#import "RCTBridgeModule.h"
#import <React/RCTEventEmitter.h>
#import <React/RCTViewManager.h>
#import <UIKit/UIKit.h>
#import "NPPosManager.h"
NS_ASSUME_NONNULL_BEGIN

@interface RNN982 : RCTEventEmitter <PosManagerDelegate, RCTBridgeModule, UITextViewDelegate,UITableViewDataSource, UIPageViewControllerDelegate> {
    UITableView        *PeripheralsTable;
    NSMutableArray     *Peripherals;
    NSArray     *ordersArray;
    UIPageViewController *picker;
}



@property (weak, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic, strong) NSMutableArray     *peripheralArray;
@property (nonatomic, retain) NSArray     *ordersArray;
@property (nonatomic,assign) int vl;
@property (nonatomic,strong) UIPageViewController *picker;

@property (nonatomic, strong) UIViewController *window;

@end

NS_ASSUME_NONNULL_END


