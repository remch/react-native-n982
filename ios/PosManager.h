

#import <Foundation/Foundation.h>
#import "PosManagerOther.h"


@protocol PosManagerDelegate <NSObject>

@optional




/*All operation exceptions, receive error codes and error descriptions returned by each interface


@Param errorcode error code

@Param errInfo error description*/



-(void)onReceiveErrorCode:(ErrorCode)errorCode errInfo:(NSString *)errinfo;






/*Firmware upgrade progress


@Progress of param process upgraded (keep two decimal places)

*/

-(void)onUpdateFirmwareProcess:(float)process;

@end


@interface PosManager : NSObject

@property (nonatomic, weak) id<PosManagerDelegate> delegate;

/**
 singleton;
 
 @return singleton;
 */
+ (instancetype)sharedPosManager;

/**
scan devices;
 
 @param timeout Scan timeout
 
 @param scanCB Scan results
 
 */
-(void)scan:(int)timeout withResult:(onScanResultCB)scanCB;

/**
 

 Stop scanning
 
 */
-(void)stopScan;

/**
 

 Connecting device
 
 
 @param uuid  Bluetooth UUID
 
 @param successCB Connection success callback
 */
-(void)connectDeviceWithUUID:(NSString *)uuid successBlock:(onVoidCB)successCB;

/**
 Judge connection status
 
 @return Yes connected no not connected
 */
-(BOOL)isConnected;

/**
 Disconnect
 
 @param successCB Disconnect callback successful
 */
-(void)disconnectDeviceSuccessBlock:(onVoidCB)successCB;

/**
 

 Get device information
 
 
 @param deviceInfoCB Get successful callback
 
 */
-(void)getDeviceInfo:(onGetDeviceInfoCB)deviceInfoCB;

/**
 

 Get transport key
 
 
 @param pubkey Public key
 
 @param getTskCB Get successful callback
 */
-(void)getTransportSessionKey:(NSString *)pubkey withResult:(onGetSessionKeyCB)getTskCB;

/**
 Update master key
 
 @param masterKey master key  16key+4kvc（6184D366D0589E501688D408745E5B20ED06C1EB）
 @param successCB Update successful callback
 */
-(void)updateMasterKey:(NSString *)masterKey successBlock:(onVoidCB)successCB;

/**
 Update work key
 
 @param workKey (pinkey, makey, key of encrypted message, each key is followed by 4-byte check bit)
 @param successCB Update successful callback
 */
-(void)updateWorkKey:(NSString *)workKey successBlock:(onVoidCB)successCB;

/**
 

 Add AID
 
 
 @param aid Application Identifier
 @param successCB Update successful callback
 */
-(void)addAid:(NSString*)aid successBlock:(onVoidCB)successCB;

/**
 

 Add RID
 
 
 @param rid Registration identifier
 @param successCB Update successful callback
 */
-(void)addRid:(NSString*)rid successBlock:(onVoidCB)successCB;

/**
 Get card number
 
 @param timeout Timeout time
 
 @param result Get successful callback
 */
-(void)getCardNumber:(int)timeout withResult:(onGetCardNumberCB)result;

/**
 Get power status
 
 @param result Get power callback
 */
-(void)getCurrentBatteryStatu:(onGetCurrentBatteryStatu)result;

/**
 Pay by card
 
 @param paramsInfo Data model for card reading
 @param cardInsertCB Callback when IC card is inserted
 @param readCardCB Callback upon completion of card reading
 */
-(void)readCard:(ReadCardParamsInfo *)paramsInfo ICCardDidInserted:(onVoidCB)cardInsertCB success:(onReadCardCB)readCardCB;

/**
 Enter amount and password
 
 @param info Input information model
 @param successCB Input successful callback
 */
-(void)getInputInfoFromKB:(InputInfo*)info successBlock:(onReadInputInfo)successCB ;

/**
 Two certification
 
 @param onLineData Response data model obtained from server
 
 @param successCB Write back successful callback
 
 */
-(void)onlineDataProcess:(EMVOnlineData *)onLineData successBlock:(onVoidCB)successCB;

/**
 Cancel transaction
 
 @param successCB Cancel successful callback
 */
-(void)cancelTrade:(onVoidCB)successCB;

/**
 Calculate MAC
 
 @param mac Mac string to evaluate
 
 @param successCB Calculation successful callback
 
 */
-(void)calculateMac:(NSString*)mac successBlock:(onNSStringCB)successCB;

/**
 Update Firmware
 
 @param path Firmware file storage path
 */
-(void)updateFirmwareWithPath:(NSString *)path;

/**
 Get QR code
 
 @param str Get QR code
 
 @param time Display time
 
 @param successCB Show successful callbacks
 
 */
-(void)getQrcoderWtihStr:(NSString *)str showTime:(int)time successBlock:(onVoidCB)successCB;

/**


Display text


@param aString Text required
@param timeOut Display time
@param successCB Show successful callbacks

*/
-(void)showScreen: (NSString *)aString  andTimeOut:(int)timeOut completeBlock:(onVoidCB)successCB;


/**


 Save data
 
 */
-(void)writeNecessaryInfo:(NSString*)info successBlock:(onVoidCB)successCB;

/**
 Read data
 */
-(void)readNecessaryInfo:(onGetInfo)successCB;


/**
clear Rid
*/
-(void)clearAidSuccessBlock:(onVoidCB)successCB;

/**
clear Rid
*/
-(void)clearRidSuccessBlock:(onVoidCB)successCB;


@end


