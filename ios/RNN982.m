
#import "RNN982.h"
#import "NPPosManager.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
//#import "PosManager.h"
#import <React/RCTLog.h>
//#import "ViewController.h"

@implementation RNN982
@synthesize peripheralArray;
@synthesize ordersArray;
@synthesize vl;
@synthesize picker;

- (dispatch_queue_t)methodQueue
{
    picker = [[UIPageViewController alloc] init];

    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

- (NSArray<NSString *> *)supportedEvents
{
  return @[@"EventReminder"];
}

RCT_EXPORT_METHOD(getDeviceInfo:(NSString *)name)//callback:(RCTResponseSenderBlock)callback)
{
    NPPosManager *Manager =[NPPosManager sharedPosManager];
    Manager.delegate  =self;
    [Manager getDeviceInfo:^(DeviceInfo *deviceInfo) {
        DeviceInfo * info= deviceInfo;
        NSString *data =[NSString stringWithFormat:@"deviceType=%d\nbtName=%@\nksn=%@\ncurrentElePer=%.1f\nfirmwareVer=%@",info.deviceType,info.btName,info.ksn ,info.currentElePer,info.firmwareVer];
        UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"deviceInfo" message: data delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alterView show];

        [self sendEventWithName:@"EventReminder" body:@{
            @"deviceType": @(info.deviceType),
            @"btName": info.btName,
            @"ksn": info.ksn == nil ? @"null" : info.ksn,
            @"currentElePer": @(info.currentElePer),
            @"firmwareVer": info.firmwareVer,
        }];

    }];


//    [self sendEventWithName:@"EventReminder" body:@{@"result": @true, @"resultResponse": @true}];

}
RCT_EXPORT_METHOD(getCardNumber:(NSString *)name)// callback:(RCTResponseSenderBlock)callback)
{
    NPPosManager *Manager =[NPPosManager sharedPosManager];
    Manager.delegate  =self;
    [Manager getCardNumber:10 withResult:^(NSString *cardNumber) {
        UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"Card" message:cardNumber delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alterView show];

        [self sendEventWithName:@"EventReminder" body:@{
            @"Card": cardNumber,
        }];
    }];
}
RCT_EXPORT_METHOD(transaction:(NSString *)name)// callback:(RCTResponseSenderBlock)callback)
{
    NPPosManager *Manager =[NPPosManager sharedPosManager];
    Manager.delegate  =self;
    ReadCardParamsInfo * info = [[ReadCardParamsInfo alloc]init];
    info.tradeType =TradeType_Deal;
    info.timeout =30;
    info.amount=@"100";
      info.supportFallBack=FALSE;
    [Manager  readCard:info ICCardDidInserted:^{

//        UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"Card Is Insert" message:@"Insert" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alterView show];

    } success:^(CardDataInfo *cardInfo) {
        NSString * info =[NSString stringWithFormat:@"cardType=%d\ntrack1=%@\ntrack2=%@\ntrack3=%@\ncardno=%@\nexperiyDate=%@\npanSerial=%@\ndata55=%@\niv=%@",cardInfo.cardType,cardInfo.track1,cardInfo.track2,cardInfo.track3,cardInfo.cardno,cardInfo.experiyDate,cardInfo.panSerialNO,cardInfo.data55,cardInfo.iv];
//        UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"getInputInfoFromKB" message:info delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alterView show];

        [self sendEventWithName:@"EventReminder" body:@{
            @"cardType": @(cardInfo.cardType),
            @"track1": cardInfo.track1,
            @"track2": cardInfo.track2,
            @"track3": cardInfo.track3,
            @"cardno": cardInfo.cardno,
            @"experiyDate": cardInfo.experiyDate,
            @"panSerialNO": cardInfo.panSerialNO,
            @"data55": cardInfo.data55,
            @"iv": cardInfo.iv == nil ? @"null" : cardInfo.iv,
            @"type": @"3"
        }];

    }];
}
RCT_EXPORT_METHOD(setInput:(NSString *)name)// callback:(RCTResponseSenderBlock)callback)
{
    NPPosManager *Manager =[NPPosManager sharedPosManager];
    Manager.delegate  =self;
    InputInfo *info =[[InputInfo alloc]init];
    info.inputType = InputTypeAmountFromKB;
    info.timeout=40;
    [Manager  getInputInfoFromKB:info successBlock:^(NSString *info) {
//        UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"getInputInfoFromKB" message:info delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alterView show];

        [self sendEventWithName:@"EventReminder" body:@{
            @"amount": info,
            @"type": @"2"
        }];
    }];
}
RCT_EXPORT_METHOD(initSdk:(NSString *)name )//callback:(RCTResponseSenderBlock)callback)
{
    dispatch_async(dispatch_get_main_queue(), ^{

        AppDelegate *share = (AppDelegate *)[UIApplication sharedApplication].delegate;
        UINavigationController *nav = (UINavigationController *) share.window.rootViewController;


//                 UIPageViewController *picker = [[UIPageViewController alloc] init];
//                 UINavigationController* contactNavigator = [[UINavigationController alloc] initWithRootViewController:picker];
//                 AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//                 [delegate.window.rootViewController presentViewController:contactNavigator animated:YES completion:nil];


//        NPPosManager *Manager =[NPPosManager sharedPosManager];
//        Manager.delegate  =self;
//        BOOL  state = [Manager isConnected];
//        NSString *data ;
//        if(state){
//            data=@"Device Is Connected";
//        }else {
//             data=@"Device Is Not Connected";
//        }
//        UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"Connection State" message: data delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//             [alterView show];


//        RCTLogInfo(@"call GetPeripheralArray **********************");


        peripheralArray =[[ NSMutableArray alloc]init];
//       self.ordersArray = @[@"Search Device ",@"Stop Search Device",@"Connect Device",@"Connection State",@"Disconnect Device",@"Get Device Information",@"Get Transport Key",@"Uptdate Master Key",@"Update Work Key ",@"Add Aid",@"Add Rid",@"Get Card Number",@"Get Power status",@"Swipe Card",@"Input Amount",@"Second Authentication",@"Cancel Transaction",@"Calculate MAC",@"Update Firmware",@"Generate QR Code",@"Store transaction Tata",@"Read Transaction Data",@"Input Password",@"Show Text",@"clean Aid",@"clean Rid"];

        NPPosManager *Manager =[NPPosManager sharedPosManager];
        Manager.delegate  =self;
        peripheralArray =[[ NSMutableArray alloc]init];
        [Manager  scan:10 withResult:^(CBPeripheral *device) {
            CBPeripheral * Peripheral=device;
            [peripheralArray addObject:device];
            [self   GetPeripheralArray:peripheralArray];
        }];

    });


//    Manager.delegate  =self;
//    [Manager connectDeviceWithUUID:@"" successBlock:^{
//    }];

//    if ([API_KEY isEqualToString:@"INSERT API KEY HERE"]) {
//        RCTLogInfo(@"You shuld add your API KEY at the top of AppDelegate.m. Line 12.");
//        //[NSException raise:@"API Key missing" format:@"You shuld add your API KEY at the top of AppDelegate.m. Line 12."];
//    } else {
//        [[LaPosSDK sharedSDK] startWithAPIKey:API_KEY];
//    }
//
//    if ([USER_TOKEN isEqualToString:@"INSERT USER TOKEN HERE"]) {
//       RCTLogInfo(@"You shuld add your USER TOKEN at the top of AppDelegate.m. Line 13.");
//       //[NSException raise:@"User Token missing" format:@"You shuld add your USER TOKEN at the top of AppDelegate.m. Line 13."];
//    } else {
//        [[LaPosSDK sharedSDK] setUserToken:USER_TOKEN];
//    }
//
//    callback(@[[NSNull null], @true]);
   //[ self sendEventWithName:@"EventReminder" body:@{@"initSdk": @true}];
}

RCT_EXPORT_METHOD(isconnected:(NSString *)name )
{
    NPPosManager *Manager =[NPPosManager sharedPosManager];
    Manager.delegate  =self;
    BOOL  state = [Manager isConnected];
    NSString *data ;
    if(state){
        data=@"Device Is Connected";
    }else {
         data=@"Device Is Not Connected";
    }
//    UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"Connection State" message: data delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//         [alterView show];

     [self sendEventWithName:@"EventReminder" body:@{
         @"connected": @(state), // == true ? @"1" : @"0",
                 @"type": @"1"
             }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 0)
    {
    return  [ordersArray count];
    }else {
      return [Peripherals count] ;
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = nil;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    UIColor *bgColor = [UIColor colorWithRed:250.0/255.0 green:250.0/255.0  blue:250.0/255.0  alpha:1.0];
    [cell setBackgroundColor:bgColor];


    if(tableView.tag==0){
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[ordersArray objectAtIndex:indexPath.row] ];

    }else {
        NSString *str = [[Peripherals objectAtIndex:indexPath.row] identifier];
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[[Peripherals objectAtIndex:indexPath.row] name]];
         cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",str];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[aTableView cellForRowAtIndexPath:indexPath];
    cell.selected=NO;
    if(aTableView.tag==0)
    {
        RCTLogInfo(@"tableView aTableView.tag==0 **********************");

//        [self sendOrder:indexPath.row];

    }else{
        NPPosManager *Manager =[NPPosManager sharedPosManager];
        Manager.delegate  =self;
        [Manager connectDeviceWithUUID: [[Peripherals objectAtIndex:indexPath.row] identifier]  successBlock:^{
            [self removefrom];
            UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"Bluetooth State" message:@"Device Is Connected" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alterView show];
        }];
    }
}
#pragma mark -
#pragma mark
#pragma mark -
-(void)removefrom
{
    [picker.navigationController dismissViewControllerAnimated:NO completion:nil];

//    for (UIView *subviews in [self.view subviews]) {
//        if (subviews.tag==1) {
//            Peripherals =[[NSMutableArray alloc] init];
//            [subviews removeFromSuperview];
//            NPPosManager *Manager =[NPPosManager sharedPosManager];
//            Manager.delegate  =self;
//            [Manager stopScan];
//        }
//    }

}

-(void)setExtraCellLineHidden: (UITableView *)tableView
{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

//-(void)sendOrder:(long )index{
//    switch (index) {
//        case 0:
//            [self scan];
//            break;
//        case 1:
//            [self stopScan];
//            break;
//        case 2:
//             [self connectDevice];
//            break;
//        case 3:
//             [self getConnectState];
//            break;
//        case 4:
//             [self disConnect];
//            break;
//        case 5:
//             [self deviceInfo];
//            break;
//        case 6:
//            [self getTransportSessionKey];
//            break;
//        case 7:
//            [self updateMasterKey];
//            break;
//        case 8:
//            [self updateWorkKey];
//           break;
//        case 9:
//            [self addAid];
//           break;
//        case 10:
//            [self addRid];
//            break;
//        case 11:
//            [self getCardNumber];
//            break;
//        case 12:
//            [self getCurrentBatteryStatus];
//            break;
//        case 13:
//            [self readCard];
//           break;
//        case 14:
//            [self getInputInfoFromKB];
//           break;
//        case 15:
//            [self onlineDataProcess];
//           break;
//        case 16:
//            [self cancelTrade];
//          break;
//        case 17:
//            [self calculateMac];
//          break;
//        case 18:
//            [self updateFirmwareWithPath];
//            break;
//        case 19:
//           [self getQrcoderWtihStr];
//            break;
//        case 20:
//            [self writeNecessaryInfo];
//            break;
//        case 21:
//            [self readNecessaryInfo];
//            break;
//        case 22:
//            [self password];
//            break;
//        case 23:
//            [self showText];
//            break;
//        case 24:
//            [self cleanAid];
//            break;
//        case 25:
//            [self cleanRid];
//            break;
//        default:
//            break;
//    }
//}

- (void)GetPeripheralArray:(NSMutableArray*)array
{
//    RCTLogInfo(@"GetPeripheralArray **********************");

    Peripherals=[NSMutableArray  arrayWithArray:array];

//    CGRect aRect = self.view.bounds;
//    aRect.origin.y = 0;
//    aRect.size.height = aRect.size.height;
//    RCTLogInfo(@"GetPeripheralArray ********************** h:%f", aRect.size.height);

    PeripheralsTable  = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) style:UITableViewStylePlain];
    PeripheralsTable.tag = 1;
    [PeripheralsTable  setDelegate:self];
    [PeripheralsTable  setDataSource:self];
    [PeripheralsTable  setRowHeight:57.0f];
    [self setExtraCellLineHidden:PeripheralsTable];
    UIView *view1      = [[UIView alloc]init];
    CGRect arect=CGRectMake(0, -20, 320, 60);
    if([[UIScreen mainScreen] bounds].size.height > 480.0f)
    {
        arect.origin.y += 20;
    }
    view1.frame        = arect;
    UILabel*lable      = [[UILabel alloc]init];
    CGRect arect0=CGRectMake(120, 5, 320, 60);
    if([[UIScreen mainScreen] bounds].size.height > 480.0f)
    {
        arect0.origin.y += 10;
    }
    lable.frame        =arect0;
    lable.text         = @"Bluetooth";
    lable.textColor    = [UIColor blueColor];
    [view1 addSubview:lable];

//    CGRect arect1=CGRectMake(self.view.frame.size.width-80, 15, 70, 40);
    CGRect arect1=CGRectMake([[UIScreen mainScreen] bounds].size.width-80, 15, 70, 40);

    if([[UIScreen mainScreen] bounds].size.height > 480.0f)
    {
        arect1.origin.y += 10;
    }
    UIButton *button = [[UIButton alloc]initWithFrame:arect1];
    [button.titleLabel setFont:[UIFont systemFontOfSize:18]];
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    NSString *return1=[NSString stringWithFormat:@"%@",   @"Return"];
    [button setTitle:return1 forState:UIControlStateNormal];
    [button addTarget:self action:@selector(removefrom)forControlEvents:UIControlEventTouchUpInside];
    [button.titleLabel setTextColor:[UIColor grayColor]];
    [view1 addSubview:button];
    PeripheralsTable.tableHeaderView = view1;
//    [self.view addSubview:PeripheralsTable];
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

    dispatch_async(dispatch_get_main_queue(), ^{


//        UIPageViewController *picker = [[UIPageViewController alloc] init];


        [picker.view addSubview:PeripheralsTable];


        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

         UINavigationController* contactNavigator = [[UINavigationController alloc] initWithRootViewController:picker];
         AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
         [delegate.window.rootViewController presentViewController:contactNavigator animated:YES completion:nil];

    });

}

-(void)onReceiveErrorCode:(ErrorCode)errorCode errInfo:(NSString *)errinfo;
{
    [self sendEventWithName:@"EventReminder" body:@{
        @"errorMessage": errinfo,
        @"error": @(true),
        @"type": @"4",
    }];

//    UIAlertView *alterView=[[UIAlertView alloc]initWithTitle:@"ErrorCode" message:errinfo delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//    [alterView show];

}

@end

