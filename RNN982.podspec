
Pod::Spec.new do |s|
  s.name         = "RNN982"
  s.version      = "1.0.0"
  s.summary      = "RNN982"
  s.description  = <<-DESC
                  react-native-n982
                   DESC
  s.homepage     = "https://github.com/github_account/react-native-n982"
  s.license      = "MIT"
  # s.license    = { :type => "MIT", :file => "FILE_LICENSE" }
  s.authors      = { "Your Name" => "yourname@email.com" }
  s.platforms    = { :ios => "9.0" }
  s.source       = { :git => "https://github.com/github_account/react-native-n982.git", :tag => "#{s.version}" }

  s.source_files = "ios/**/*.{h,m,swift}"
  s.requires_arc = true

  s.dependency "React"
  # ...
  # s.dependency "..."
end
