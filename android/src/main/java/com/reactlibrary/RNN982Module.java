package com.reactlibrary;

import com.newpos.mposlib.bluetooth.BluetoothService;
import android.content.Intent;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import androidx.annotation.Nullable;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.ActivityEventListener;
import android.app.Activity;
import com.newpos.mposlib.sdk.NpPosManager;
import com.newpos.mposlib.sdk.INpSwipeListener;
import com.newpos.mposlib.sdk.CardInfoEntity;
import com.newpos.mposlib.sdk.DeviceInfoEntity;
import android.os.Handler;
import android.os.Looper;
import android.bluetooth.BluetoothDevice;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;
import android.util.Log;
import android.text.TextUtils;
import com.reactlibrary.utils.TDesUtil;
import com.newpos.mposlib.util.ISOUtil;
import com.reactlibrary.utils.LogUtil;
import java.security.PrivateKey;
import javax.crypto.Cipher;
import com.reactlibrary.utils.StringUtil;
import com.newpos.mposlib.sdk.CardReadEntity;
import com.newpos.mposlib.sdk.InputInfoEntity;

public class RNN982Module extends ReactContextBaseJavaModule implements ActivityEventListener, INpSwipeListener {

  private final ReactApplicationContext reactContext;
  private NpPosManager posManager;
  private final static String TAG = "OperatingActivity";
  String trackkey="C305C4F9B5B84E6CE0A3789FF822101E"; //clear key

  public RNN982Module(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
    reactContext.addActivityEventListener(this); //Register this native module as Activity result listener

  }

  @Override
  public void initialize() {
    super.initialize();
  }

  @Override
  public String getName() {
    return "RNN982";
  }

  @ReactMethod
  public void initSdk(String stringArgument) {
      ReactApplicationContext context = this.reactContext;
      Intent intent = new Intent(context, BluetoothActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(intent);
//     WritableMap params = Arguments.createMap();
//     params.putBoolean("initSdk", true);
//     sendEvent("EventReminder", params);
  }
  private void sendEvent(String eventName,
                         @Nullable WritableMap params) {
    reactContext
            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
            .emit(eventName, params);
  }
  @Override
  public void onNewIntent(Intent intent) {

  }
  @Override
  public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {

  }
  @ReactMethod
  void navigateToExample() {
    ReactApplicationContext context = this.reactContext;
    Intent intent = new Intent(context, BluetoothActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(intent);
  }

  @ReactMethod
  void getDeviceInfo(String stringArgument) {
    posManager = NpPosManager.sharedInstance(this.reactContext, this);

    posManager.getDeviceInfo();
  }
  @ReactMethod
    void isconnected(String stringArgument) {
//       posManager = NpPosManager.sharedInstance(this.reactContext, this);
//       posManager.isConnected();
            final WritableMap params = Arguments.createMap();
            params.putString("type", "1");
            params.putBoolean("connected", BluetoothService.I().isConnected());
            sendEvent("EventReminder", params);
    }
  @ReactMethod
  void getCardNumber(String stringArgument) {

    posManager = NpPosManager.sharedInstance(this.reactContext, this);

    CardReadEntity cardReadEntity =  new CardReadEntity();
    cardReadEntity.setSupportFallback(false);
    cardReadEntity.setTimeout(30);
    cardReadEntity.setAmount("000000000000");
    cardReadEntity.setTradeType(1);
    posManager.readCard(cardReadEntity);


  }
  @ReactMethod
  void transaction(String stringArgument) {

    posManager = NpPosManager.sharedInstance(this.reactContext, this);

    CardReadEntity cardReadEntitys =  new CardReadEntity();
    cardReadEntitys.setSupportFallback(false);
    cardReadEntitys.setTimeout(30);
    cardReadEntitys.setAmount("000000000001");
    cardReadEntitys.setTradeType(0);
    posManager.readCard(cardReadEntitys);
  }
  @ReactMethod
  void setInput(String stringArgument) {

    posManager = NpPosManager.sharedInstance(this.reactContext, this);
    InputInfoEntity inputInfoEntity = new InputInfoEntity();
    inputInfoEntity.setInputType(0);
    inputInfoEntity.setTimeout(30);
    posManager.getInputInfoFromKB(inputInfoEntity);
  }
  @Override
  public void onGetDeviceInfo(final DeviceInfoEntity deviceInfoEntity) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetDeviceInfo()");

        WritableMap params = Arguments.createMap();
        params.putBoolean("onGetDeviceInfo", true);
        params.putString("device_type", deviceInfoEntity.getDeviceTypeStr());
        params.putString("device_mac", deviceInfoEntity.getKsn());
        params.putString("device_battery", String.valueOf(deviceInfoEntity.getCurrentElePer()));
        params.putString("device_version", deviceInfoEntity.getFirmwareVer());
        sendEvent("EventReminder", params);


//        editText.append(getApplicationContext().getString(R.string.device_type)+deviceInfoEntity.getDeviceTypeStr()+"\n");//deviceInfoEntity.getDeviceType()+"\n");
//        editText.append(getApplicationContext().getString(R.string.device_mac)+deviceInfoEntity.getKsn()+"\n");
//        editText.append(getApplicationContext().getString(R.string.device_battery)+deviceInfoEntity.getCurrentElePer()+"\n");
//        editText.append(getApplicationContext().getString(R.string.device_version)+deviceInfoEntity.getFirmwareVer()+"\n");
//        setTextViewLast();
      }
    });
  }
  public String KEK=null;
  @Override
  public void onGetTransportSessionKey(final String s) {
    try {
      LogUtil.d("s:" + s);
      LogUtil.e("KCV =" +s.substring(s.length()-8));
      String kcv=s.substring(s.length()-8);
      PrivateKey privateKey = RSAUtil.getPrivateKey(RSAUtil.PRIVATE_KEY_PATH);

      Cipher cipher = Cipher.getInstance("RSA");//PKCS1Padding
      cipher.init(Cipher.DECRYPT_MODE, privateKey);
      LogUtil.d("string ==" + s.substring(0,s.length()-8 ));
      byte[] bytes = cipher.doFinal(StringUtil.hexStr2Bytes(s.substring(0,s.length()-8 )));//256 ->512
      LogUtil.d("decrypt result==" + ISOUtil.byte2hex(bytes));
      //           final String kek = StringUtil.byte2HexStr(bytes);
      //       final String kek =s.substring(s.length()-32-8,s.length()-8);
      byte []kekbyte=new byte[16];
      byte []IV=ISOUtil.hex2byte("0000000000000000");
      System.arraycopy(bytes,bytes.length-16,kekbyte,0,16);
      LogUtil.d("kek:" + ISOUtil.byte2hex(kekbyte));
      byte[]calckcv=TDesUtil.encryptECB(kekbyte,IV);
      LogUtil.d("calckcv:" + ISOUtil.byte2hex(calckcv));
      LogUtil.d("KCV:" +kcv);
      if(!ISOUtil.memcmp(calckcv,0,ISOUtil.hex2byte(kcv),0,4)){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override
          public void run() {
//            editText.append(getApplicationContext().getString(R.string.kek_unpack_fail) + "\n");
//            setTextViewLast();
          }

        });
        return ;
      }
      KEK= ISOUtil.byte2hex(kekbyte);
      new Handler(Looper.getMainLooper()).post(new Runnable() {
        @Override
        public void run() {
//          editText.append(getApplicationContext().getString(R.string.kek_unpack_success)+KEK+"\n");setTextViewLast();
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
      new Handler(Looper.getMainLooper()).post(new Runnable() {
        @Override
        public void run() {
//          editText.append(getApplicationContext().getString(R.string.kek_unpack_fail)+ "\n");setTextViewLast();
        }
      });
      return;
    }

    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetTransportSessionKey()");
        //      editText.append(getApplicationContext().getString(R.string.tmk)+s+"\n");setTextViewLast();
      }
    });
  }
  @Override
  public void onReceiveErrorCode(final int i, final String s) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onReceiveErrorCode()");
        WritableMap params = Arguments.createMap();
        params.putBoolean("onReceiveErrorCode", true);
        params.putString("errorMessage", s);
        params.putBoolean("error", true);
        params.putString("type", "4");
        sendEvent("EventReminder", params);
//        Toast.makeText(OperatingActivity.this, s, Toast.LENGTH_SHORT).show();
      }
    });
  }
  @Override
  public void onDisplayTextOnScreenSuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onDisplayTextOnScreenSuccess()");
        WritableMap params = Arguments.createMap();
        params.putBoolean("onDisplayTextOnScreenSuccess", true);
        params.putString("message", "hola");
        sendEvent("EventReminder", params);

//        editText.append(getApplicationContext().getString(R.string.display_text_success));setTextViewLast();
      }
    });
  }
  @Override
  public void onGetTransactionInfoSuccess(final String s) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetTransactionInfoSuccess()");
        WritableMap params = Arguments.createMap();
        params.putBoolean("onDisplayTextOnScreenSuccess", true);
        params.putString("message", s);
        sendEvent("EventReminder", params);
//        editText.append(getApplicationContext().getString(R.string.read_memory)+s+"\n");setTextViewLast();
//        Toast.makeText(OperatingActivity.this, "" + s, Toast.LENGTH_SHORT).show();
      }
    });
  }
  @Override
  public void onUpdateMasterKeySuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onUpdateMasterKeySuccess()");
//        editText.append(getApplicationContext().getString(R.string.update_master_key_success)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onUpdateWorkingKeySuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onUpdateWorkingKeySuccess()");
//        editText.append(getApplicationContext().getString(R.string.update_working_key_success)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onAddAidSuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onAddAidSuccess()");
//        editText.append(getApplicationContext().getString(R.string.add_aid_success)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onAddRidSuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onAddRidSuccess()");
//        editText.append(getApplicationContext().getString(R.string.add_rid_success)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onClearAids() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onClearAids()");
//        editText.append(getApplicationContext().getString(R.string.clear_aid_success)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onClearRids() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onClearAids()");
//        editText.append(getApplicationContext().getString(R.string.clear_rid_success)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onGetCardNumber(final String s) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetCardNumber()");
//        editText.append(getApplicationContext().getString(R.string.card_number)+s+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onGetDeviceBattery(final boolean b) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetDeviceBattery()");
//        editText.append((b?getApplicationContext().getString(R.string.battery_full):getApplicationContext().getString(R.string.battery_not_enough))+"\n");setTextViewLast();
      }
    });
  }
  @Override
  public void onScannerResult(BluetoothDevice bluetoothDevice) {

  }

  @Override
  public void onDeviceConnected() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
            WritableMap params = Arguments.createMap();
            params.putBoolean("connected", true);
            params.putString("type", "1");
            sendEvent("EventReminder", params);
//        Toast.makeText(OperatingActivity.this, getApplicationContext().getString(R.string.device_connect_success), Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public void onDeviceDisConnected() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
            WritableMap params = Arguments.createMap();
            params.putBoolean("connected", false);
            params.putString("type", "1");
            sendEvent("EventReminder", params);
//        Toast.makeText(OperatingActivity.this, getApplicationContext().getString(R.string.device_disconnect), Toast.LENGTH_SHORT).show();
      }
    });
  }
  @Override
  public void onDetachedIC() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onDetachedIC()");
//        editText.append(getApplicationContext().getString(R.string.detect_IC_insert)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onGetReadCardInfo(final CardInfoEntity cardInfoEntity) {
    Log.d(TAG, "Card type" + cardInfoEntity.getCardType());
    Log.d(TAG, "PIN:" + cardInfoEntity.getEncryptPin());
    Log.d(TAG, "expDate:" + cardInfoEntity.getExpDate());
    Log.d(TAG, "Card sec" + cardInfoEntity.getCsn());
    Log.d(TAG, "ic55Data:" + cardInfoEntity.getIc55Data());
    Log.d(TAG, "tusn:" + cardInfoEntity.getTusn());
    Log.d(TAG, "encryptedSN:" + cardInfoEntity.getEncryptedSN());
    Log.d(TAG, "deviceType:" + cardInfoEntity.getDeviceType());
    Log.d(TAG, "ksn:" + cardInfoEntity.getKsn());

    final WritableMap params = Arguments.createMap();
    params.putString("type", "3");


    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetReadCardInfo()");
        if(!TextUtils.isEmpty(cardInfoEntity.getTrack1())) {
          params.putString("track1", cardInfoEntity.getTrack1());

//          editText.append(getApplicationContext().getString(R.string.track1) + cardInfoEntity.getTrack1() + "\n");
        }
        if(!TextUtils.isEmpty(cardInfoEntity.getTrack2())) {
          String track2=cardInfoEntity.getTrack2();
          byte[]result=TDesUtil.descryptECB(ISOUtil.hex2byte(trackkey),ISOUtil.hex2byte(track2));
          params.putString("track2", ISOUtil.byte2hex(result));
          params.putString("track22", cardInfoEntity.getTrack2());

//          editText.append(getApplicationContext().getString(R.string.track2) +ISOUtil.byte2hex(result)+ "\n");
          //           editText.append(getApplicationContext().getString(R.string.track2) + cardInfoEntity.getTrack2() + "\n");
        }
        if(!TextUtils.isEmpty(cardInfoEntity.getTrack3())) {
          String track3=cardInfoEntity.getTrack3();
          byte[]result=TDesUtil.descryptECB(ISOUtil.hex2byte(trackkey),ISOUtil.hex2byte(track3));
          params.putString("track3", ISOUtil.byte2hex(result));
          params.putString("track33", cardInfoEntity.getTrack3());

//          editText.append(getApplicationContext().getString(R.string.track3) +ISOUtil.byte2hex(result)+ "\n");
          //              editText.append(getApplicationContext().getString(R.string.track3) + cardInfoEntity.getTrack3() + "\n");
        }
        if(!TextUtils.isEmpty(cardInfoEntity.getCardNumber())) {
          params.putBoolean("onGetReadCardInfo", true);
          params.putString("number", cardInfoEntity.getCardNumber());
//          editText.append(getApplicationContext().getString(R.string.card_no)+cardInfoEntity.getCardNumber()+"\n");
        }
        if(!TextUtils.isEmpty(cardInfoEntity.getEncryptedSN())) {
          params.putString("encryptedSN", cardInfoEntity.getEncryptedSN());
//          editText.append("encryptedSN:"+cardInfoEntity.getEncryptedSN()+"\n");
        }
        sendEvent("EventReminder", params);

      }
    });
/***
 InputInfoEntity inputInfoEntityd = new InputInfoEntity();
 inputInfoEntityd.setInputType(1);
 inputInfoEntityd.setTimeout(35);
 inputInfoEntityd.setPan(cardInfoEntity.getCardNumber());
 posManager.getInputInfoFromKB(inputInfoEntityd);
 ***/
  }
  @Override
  public void onGetReadInputInfo(final String s) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetReadInputInfo()");
        final WritableMap params = Arguments.createMap();
        params.putString("type", "2");
        params.putBoolean("onGetReadInputInfo", true);
        params.putString("amount", s);
        sendEvent("EventReminder", params);

//        editText.append(getApplicationContext().getString(R.string.amount_or_pin)+s+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onGetICCardWriteback(final boolean b) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetICCardWriteback()");
//        editText.append(getApplicationContext().getString(R.string.second_gac)+(b?getApplicationContext().getString(R.string.auth_success):getApplicationContext().getString(R.string.auth_fail))+"\n");
      }
    });
  }

  @Override
  public void onCancelReadCard() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onCancelReadCard()");
//        editText.append(getApplicationContext().getString(R.string.cancel_tran_success)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onGetCalcMacResult(final String s) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGetCalcMacResult()");
//        editText.append("Mac :"+s+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onUpdateFirmwareProcess(final float v) {
    Log.d(TAG,"onUpdateFirmwareProcess " + v);
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onUpdateFirmwareProcess " + v);
//        editText.setText(getApplicationContext().getString(R.string.update)+ v +"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onUpdateFirmwareSuccess() {
    Log.d(TAG,"onUpdateFirmwareSuccess ");
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onUpdateFirmwareSuccess ");
//        editText.append(getApplicationContext().getString(R.string.update_success) +"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onGenerateQRCodeSuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onGenerateQRCodeSuccess()");
//        editText.append(getApplicationContext().getString(R.string.generate_code_success)+"\n");setTextViewLast();
      }
    });
  }

  @Override
  public void onSetTransactionInfoSuccess() {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        Log.d(TAG,"onAddAidSuccess()");
//        editText.append(getApplicationContext().getString(R.string.save_memory_success)+"\n");setTextViewLast();
      }
    });
  }

}
